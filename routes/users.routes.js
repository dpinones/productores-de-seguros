const express = require('express');
const router = express.Router();
const { isAuthenticated } = require("../helpers/auth");

const {
    singup,
    signin,
    logout
} = require("../controllers/users.controller");

// Routes
router.post("/signup", singup);
router.post("/signin", signin);
router.get("/logout", isAuthenticated, logout);

module.exports = router;
