const express = require('express');
const router = express.Router();
const { isAuthenticated } = require("../helpers/auth");

const {
    getAllContacts,
    getContact,
    createContact,
    updateContact,
    deleteContact
} = require('../controllers/contact.controller');

router.get('/', isAuthenticated, getAllContacts);
router.get('/:id', isAuthenticated, getContact);
router.post('/', isAuthenticated, createContact);
router.put('/:id', isAuthenticated, updateContact);
router.delete('/:id', isAuthenticated, deleteContact);

module.exports = router;
