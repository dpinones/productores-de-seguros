const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/home', function(req, res, next) {
  res.send("Bienvenido al home!");
});

router.get('/error', function(req, res, next) {
  res.send("Error!");
});

module.exports = router;
