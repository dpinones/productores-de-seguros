const helpers = {};

helpers.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  res.status(400).send('error_msg - Not Authorized.');
  // res.redirect('/users/signin');
};

module.exports = helpers;
