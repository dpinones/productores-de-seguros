const usersCtrl = {};
const User = require('../models/User');
const passport = require("passport");


usersCtrl.singup = async (req, res) => {
  const { name, email, password, confirm_password } = req.body;
  if (password !== confirm_password) {
    res.status(400).send("Passwords do not match.");
  }
  if (password.length < 4) {
    res.status(400).send("Passwords must be at least 4 characters.");
  }

  // Look for email coincidence
  const emailUser = await User.findOne({ email: email });
  if (emailUser) {
    res.status(400).send("error_msg - The Email is already in use.");
  } else {
    // Saving a New User
    const newUser = new User({ name, email, password });
    newUser.password = await newUser.encryptPassword(password);
    await newUser.save();
    res.status(200).send("success_msg - You are registered.");
  }
};

usersCtrl.signin = passport.authenticate("local", {
    successRedirect: "/home",
    failureRedirect: "/error",
  });

usersCtrl.logout = (req, res) => {
  req.logout();
  res.status(200).send("success_msg - You are logged out now.");
};

module.exports = usersCtrl;
