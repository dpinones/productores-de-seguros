const contactsCtrl = {};

const ObjectId = require('mongoose').Types.ObjectId;
const Contact = require('../models/Contact');

contactsCtrl.getAllContacts = async (req, res) => {
    Contact.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Contact :' + JSON.stringify(err, undefined, 2)); }
    });
};

contactsCtrl.getContact = async (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Contact.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Contact :' + JSON.stringify(err, undefined, 2)); }
    });
};

contactsCtrl.createContact = async (req, res) => {
    var contact = new Contact({
        name: req.body.name,
        numberPhone: req.body.numberPhone,
        email: req.body.email,
    });
    contact.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Contact Save :' + JSON.stringify(err, undefined, 2)); }
    });
};

contactsCtrl.updateContact = async (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var contact = {
        name: req.body.name,
        numberPhone: req.body.numberPhone,
        email: req.body.email,
    };
    Contact.findByIdAndUpdate(req.params.id, { $set: contact }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Contact Update :' + JSON.stringify(err, undefined, 2)); }
    });
};

contactsCtrl.deleteContact = async (req, res) => {
    console.log("req.params.id = ", req.params.id);
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Contact.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Contact Delete :' + JSON.stringify(err, undefined, 2)); }
    });
};

module.exports = contactsCtrl;
