const { Schema, model } = require("mongoose");

const ContactSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        numberPhone: {
            type: Number,
            required: true
        },
        email: {
            type: String,
            required: false
        }
    },
    {
        timestamps: true
    }
);

module.exports = model("Contact", ContactSchema);
